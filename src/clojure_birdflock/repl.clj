(ns clojure-birdflock.core
  (:use [clojure.algo.generic.functor :only (fmap)]
        [clojure.pprint]
        [criterium.core])
  (:require [clojure.math.numeric-tower :as math]
            [cljs.core :exclude [unsigned-bit-shift-right]]
            [clojure.math.combinatorics :as combo]
            [quil.core :as q]
            [quil.middleware :as m]))

;Generate a random flock
(def spawn-box-size 200)

(defn generate-bird
  "bird is a map with vects: :p [x y z] :v [vx vy vz]; speed in km/h"
  []
  {:position (repeatedly 3 #(+ (rand-int spawn-box-size) 1))
   :velocity (repeatedly 3 #(+ (rand-int 4) 5))}) ;max 50

(defn generate-flock
  "generates a flock of given size"
  [size]
  (doall (repeatedly size generate-bird)))

;Some constants
(def flock-size 40)
(def dt 0.1)
(def opt-dist 100)
(def max-velocity 60)
(def max-acceleration 3)
(def PID-constants {:P 0.2 :I 0 :D 0.3})
(def n 2)
(def PID-weights ;must add to 1
  {:avg-veloc 0.5
   :col 0.5})


(def flock (atom (generate-flock flock-size)))
;Bottoms up computation

(defn next-position
  "returns the next position of a bird"
  [b]
  (let [dx (map #(* dt %) (:velocity b))]
    (map + (:position b) dx)))

(defn vect-diff
  [v1 v2]
  (map - v1 v2))

(defn dist-vect
  "vect pointing b1 away from b2; b1 <--dv-- b2"
  [b1 b2]
  (vect-diff (:position b1) (:position b2)))

(defn next-dist-vect
  "same as dist-vect but for next-position"
  [b1 b2]
  (vect-diff (next-position b1) (next-position b2)))

(defn velocity-vect
  [b1 b2]
  (vect-diff (:velocity b1) (:velocity b2)))

(defn magn
  "computes the magn from the dist-vect"
  [v]
  (math/sqrt (reduce + (map #(math/expt % 2) v))))

(defn dist
  "gives the dist between two birds"
  [b1 b2]
  (magn (dist-vect b1 b2)))

(defn error
  "difference between optimum and real dist; [-inf, inf]. error is positive when birds are too close. this works because dv is calculated as pointing away"
  [dv & {:keys [opt-dist] :or {opt-dist opt-dist}}]
    (- opt-dist (magn dv)))

(defn avg-velocity-vect
  "Computes the average velocity vect of a group of birds."
  [birds]
  (let [velocs (map #(:velocity %) birds)]
    (map #(/ % (count birds)) (apply map + velocs))))

(defn PID
  "PID value: P + I + D. cutoff at 1; [-1,1]"
  [dv1 dv2]
  (let [e1 (error dv1)
        e2 (error dv2)
        dvdt (/ (- e2 e1) dt)] ;differentiation
    (let [PID (+
      (* (:P PID-constants) e2)
      (* (:I PID-constants) (+ (* e1 dt) (/ (* dvdt dt) 2))) ;integration
      (* (:D PID-constants) dvdt))]
      (cond
        (> 1 PID) 1
        (> -1 PID) -1
        :else PID))))

(defn norm
  "normalizes a vect"
  [dv]
  (let [m (magn dv)]
    (if (== m 0)
      (throw (Throwable. "Error when normalizing: null vect"))
      (map #(/ % m) dv))))

(defn scalar-*
  "multiplies a scalar and a vect (map)"
  [scalar v]
  (map #(* scalar %) v))

(defn correction
  "PID (scalar) * max-acceleration (scalar) * norm([dvx, dvy, dvz]) (vect)"
  [dv1 dv2]
  (scalar-* (* (PID dv1 dv2) max-acceleration dt)
              (norm dv2)))

(defn collision-correction
  [b neighbours]
  (map #(/ % (count neighbours)) ;normalize by number of birds
    (reduce #(map + %1 %2) ;combine by adding
      (map #(correction
             (dist-vect b %) ;dv1
             (next-dist-vect b %)) ;dv2
             neighbours))))
(defn delta-velocity
  "computes delta velocity from individual influences"
  [b neighbours]
  (let [diff-avg (vect-diff (avg-velocity-vect neighbours) (:velocity b))]
    (map +
         (scalar-* (:avg-veloc PID-weights) (correction diff-avg diff-avg)) ;avg-velocity
         (scalar-* (:col PID-weights) (collision-correction b neighbours))))) ;collision

(defn update-velocity
  "combines corrections of all neighbours of the bird. dv denotes deltavelocity here"
  [b neighbours]
  (let [dv (delta-velocity b neighbours)]
    (let [updated-velocity (map + (:velocity b) dv)]
      (if (> (magn updated-velocity) max-velocity)
        (assoc-in b [:velocity] (scalar-* max-velocity (norm updated-velocity)))
        (assoc-in b [:velocity] updated-velocity)))))

(def flock (generate-flock 20))
(def b (generate-bird))
(next-position b)
