(ns clojure-birdflock.core
  (:use [clojure.algo.generic.functor :only (fmap)]
        [clojure.pprint]
        [criterium.core])
  (:require [clojure.math.numeric-tower :as math]
            [cljs.core :exclude [unsigned-bit-shift-right]]
            [clojure.math.combinatorics :as combo]
            [quil.core :as q]
            [quil.middleware :as m]))

;Generate a random flock
(def spawn-box-size 200)

(defn generate-bird
  "bird is a map with vects: :p [x y z] :v [vx vy vz]; speed in km/h"
  []
  {:coords (repeatedly 3 #((partial + 1) (rand-int spawn-box-size)))
   :veloc (repeatedly 3 #((partial + 5) (rand-int 4)))}) ;max 50

(defn generate-flock
  "generates a flock of given size"
  [size]
  (doall (repeatedly size generate-bird)))

;Some constants
(def flock-size 40)
(def dt 0.2)
(def opt-dist 5)
(def max-veloc 60)
(def max-acceleration 30)
(def PID-constants {:P 0.2 :I 0.1 :D 0.1})
(def n 7)
(def PID-weights ;must add to 1
  {:avg-veloc 0.7
   :collision 0.3})


(def flock (atom (generate-flock flock-size)))
;Bottoms up computation

(defn next-coords
  "returns the next coords of a bird"
  [b]
  (let [dx (map (partial * dt) (:veloc b))]
    (map + (:coords b) dx)))

(defn vect-diff
  [v1 v2]
  (map - v1 v2))

(defn dist-vect
  "vect pointing b1 away from b2; b1 <--dv-- b2"
  [b1 b2]
  (vect-diff (:coords b1) (:coords b2)))

(defn next-dist-vect
  "same as dist-vect but for next-coords"
  [b1 b2]
  (vect-diff (next-coords b1) (next-coords b2)))

(defn veloc-vect
  [b1 b2]
  (vect-diff (:veloc b1) (:veloc b2)))

(defn magn
  "computes the magn from the dist-vect"
  [v]
  (math/sqrt (reduce + (map #(math/expt % 2) v))))

(defn dist
  "gives the dist between two birds"
  [b1 b2]
  (magn (dist-vect b1 b2)))

(defn error
  "difference between optimum and real dist; [-inf, inf]. error is coordsitive when birds are too close. this works because dv is calculated as pointing away"
  [dv & {:keys [opt-dist] :or {opt-dist opt-dist}}]
    (- opt-dist (magn dv)))

(defn avg-veloc-vect
  "Computes the average veloc vect of a group of birds."
  [birds]
  (let [velocs (map (partial :veloc) birds)]
    (map #(/ % (count birds)) (apply map + velocs))))

(defn PID
  "PID value: P + I + D. cutoff at 1; [-1,1]"
  [dv1 dv2]
  (let [e1 (error dv1)
        e2 (error dv2)
        dvdt (/ (- e2 e1) dt)] ;differentiation
    (let [PID (+
      (* (:P PID-constants) e2)
      (* (:I PID-constants) (+ (* e1 dt) (/ (* dvdt dt) 2))) ;integration
      (* (:D PID-constants) dvdt))]
      (cond
        (> 1 PID) 1
        (> -1 PID) -1
        :else PID))))

(defn norm
  "normalizes a vect"
  [dv]
  (let [m (magn dv)]
    (if (== m 0)
      (throw (Throwable. "Error when normalizing: null vect"))
      (map #(/ % m) dv))))

(defn scalar-*
  "multiplies a scalar and a vect (map)"
  [scalar v]
  (map (partial * scalar) v))

(defn correction
  "PID (scalar) * max-acceleration (scalar) * norm([dvx, dvy, dvz]) (vect)"
  [dv1 dv2]
  (scalar-* (* (PID dv1 dv2) max-acceleration dt)
              (norm dv2)))

(defn collision-correction
  "anti-collision correction term"
  [b neighbours]
  (map #(/ % (count neighbours)) ;normalize by number of birds
    (reduce #(map + %1 %2) ;combine by adding
      (map #(correction
             (dist-vect b %) ;current dist-vect
             (next-dist-vect b %)) ;projected dist-vect
             neighbours))))

(defn avg-veloc-correction
  "correction term for the average veloc"
  [b neighbours]
  (let [diff-avg (vect-diff (avg-veloc-vect neighbours) (:veloc b))]
    (correction diff-avg diff-avg)))

(defn delta-veloc
  "computes delta veloc from individual influences"
  [b neighbours]
  (map +
         (scalar-* (:avg-veloc PID-weights) (avg-veloc-correction b neighbours)) ;avg-veloc
         (scalar-* (:collision PID-weights) (collision-correction b neighbours)))) ;collision

(defn update-veloc
  "combines corrections of all neighbours of the bird. dv denotes deltaveloc here"
  [b neighbours]
  (let [dv (delta-veloc b neighbours)]
    (let [updated-veloc (map + (:veloc b) dv)]
      (if (> (magn updated-veloc) max-veloc)
        (assoc-in b [:veloc] (scalar-* max-veloc (norm updated-veloc)))
        (assoc-in b [:veloc] updated-veloc)))))

(defn update-coords
  "updates a bird's coords using its veloc and dt"
  [b]
  (assoc-in b [:coords] (next-coords b)))

;proximity
(defn sphere-of-influence?
  "checks whether b2 is within sphere of influence of b1; returns bool. returns false if b1 = b2"
  [b1 b2]
  true)

(defn neighbours
  [b other-bs]
  (filter (partial sphere-of-influence? b) other-bs))

(defn nearer?
  "implements IComparator; nearer defined in Euclidean distance"
  [b1 b2 b]
  (if (= b1 b2)
    0
    (let [d1 (dist b b1)
          d2 (dist b b2)]
      (if (>= d1 d2)
        1
        -1))))

(defn n-nearest
  "selects nearest neighbour"
  [b other-bs n]
    (take n (sort #(nearer? %1 %2 b) other-bs)))

;distribute birds
(defn bird-step
  "find neighbours -> update coords -> update veloc"
  [b flock]
  (let [other-bs (remove (partial = b) flock)]
    (let [nb (n-nearest b other-bs n)]
      (if (empty? nb)
        (throw (Throwable. "Zero neighbours"))
        (update-veloc (update-coords b) nb)))))

(defn step
  "steps the simulation. a flock is list of birds (vects). might take benefit from parallelization"
  [flock]
  (for [b flock]
    (bird-step b flock)))

(defn simulate
  []
  "performs a lazy simulation"
  (repeatedly #(swap! flock step)))

;testing

;animating
(defn draw [bird-data]
  (q/background 255)

  ;rotate camera by 1 angle
  (q/begin-camera)
  (q/rotate-z (* (/ 0.5 360) 6.28))
  (q/end-camera)

  ;draw plane and lines
  (q/stroke-weight 2)
  (q/fill 200)
  (q/rect 0 0 1000 1000)
  (q/line 0 0 0 0 0 1000)
  (q/line 0 0 0 0 2000 0)
  (q/line 0 0 0 2000 0 0)

  ;draw birds
  (q/fill 0)
  (q/stroke-weight 4)
  (doseq [b (first bird-data)]
    (apply q/line (concat (:coords b) (map + (:veloc b) (:coords b))))))

(defn setup []
  (q/camera 2000 1800 1700 500 500 500 0 0 -1)
  (q/frame-rate 60)
  (simulate))

(defn update [bird-data]
  (rest bird-data))

(q/defsketch birdflock
  :draw draw
  :size [1000 1000]
  :renderer :p3d
  :setup setup
  :update update
  :middleware [m/fun-mode])

