(ns clojure-birdflock.predator
  (:use clojure-birdflock.core))

(defn generate-predator
  "generates a predator: {:coords, :veloc, :target}"
  []
  (assoc (generate-bird) :target nil))

(generate-predator)




